package com.onwelo.perla.lublin.hackaton.model.entity;

public enum EventType {
    SINGLE, DOUBLE, MULTIPLE
}
