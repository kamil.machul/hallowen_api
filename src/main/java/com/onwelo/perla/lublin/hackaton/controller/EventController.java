package com.onwelo.perla.lublin.hackaton.controller;

import com.onwelo.perla.lublin.hackaton.exceptions.ResultNotFoundException;
import com.onwelo.perla.lublin.hackaton.mapper.EventMapper;
import com.onwelo.perla.lublin.hackaton.mapper.UserMapper;
import com.onwelo.perla.lublin.hackaton.model.dto.EventDto;
import com.onwelo.perla.lublin.hackaton.model.dto.UserDto;
import com.onwelo.perla.lublin.hackaton.model.entity.Event;
import com.onwelo.perla.lublin.hackaton.model.entity.EventType;
import com.onwelo.perla.lublin.hackaton.service.EventService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("events")
public class EventController {
    private EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping
    public List<EventDto> getAll() {
        return eventService.findAll().stream()
                .map(EventMapper.INSTANCE::eventToEventDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<EventDto> addEvent(@RequestParam Long reporterId,
                                             @RequestParam EventType eventType,
                                             @RequestParam int multiplier,
                                             @RequestParam String description,
                                             @RequestParam Float longitude,
                                             @RequestParam Float latitude) throws ResultNotFoundException {
        return ResponseEntity.ok().body(EventMapper.INSTANCE.eventToEventDto(
                eventService.addEvent(reporterId, eventType, multiplier, description, longitude, latitude)));
    }

    @PutMapping("/{eventId}/member/{memberId}")
    public ResponseEntity<EventDto> addMember(@PathVariable Long eventId,
                                              @PathVariable Long memberId) throws ResultNotFoundException {
        return ResponseEntity.ok().body(EventMapper.INSTANCE.eventToEventDto(
                eventService.addMemberToEvent(memberId, eventId)));
    }

    @PutMapping("/{eventId}/member/{memberId}/finish")
    public ResponseEntity<EventDto> finishEvent(@PathVariable Long eventId,
                                                @PathVariable Long memberId) throws ResultNotFoundException {
        return ResponseEntity.ok().body(EventMapper.INSTANCE.eventToEventDto(
                eventService.finishEvent(memberId, eventId)));
    }
}
