package com.onwelo.perla.lublin.hackaton.model.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Event {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private EventType eventType;

    @Column
    private String description;

    @Column
    private int multiplier;

    @Column
    private boolean isActive;

    @Column
    private float longitude;

    @Column
    private float latitude;

    @OneToOne(fetch = FetchType.EAGER)
    private User reporter;

    @OneToMany(fetch = FetchType.EAGER)
    private List<User> members;
}
