package com.onwelo.perla.lublin.hackaton.service;

import com.onwelo.perla.lublin.hackaton.exceptions.DuplicatedResourceException;
import com.onwelo.perla.lublin.hackaton.exceptions.ResultNotFoundException;
import com.onwelo.perla.lublin.hackaton.mapper.UserMapper;
import com.onwelo.perla.lublin.hackaton.model.dto.UserDto;
import com.onwelo.perla.lublin.hackaton.model.entity.Role;
import com.onwelo.perla.lublin.hackaton.model.entity.User;
import com.onwelo.perla.lublin.hackaton.repository.UsersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UsersService {
    UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<User> findAll() {
        return usersRepository.findAll();
    }

    public User addUser(UserDto user) {
        return usersRepository.save(UserMapper.INSTANCE.userDtoToUser(user));
    }

    public User getUser(long id) throws ResultNotFoundException {
        return usersRepository.findById(id).orElseThrow(() -> new ResultNotFoundException("User does not found"));
    }

    public User addMonster(UserDto user) throws DuplicatedResourceException {
        if (usersRepository.existsUsersByRole(Role.ROLE_MONSTER)) {
            throw new DuplicatedResourceException("Monster already exists");
        }
        return usersRepository.save(UserMapper.INSTANCE.userDtoToUser(user));
    }

    public User updateLocation(Long userId, Float longitude, Float latitude) throws ResultNotFoundException {
        User user = usersRepository.findById(userId).orElseThrow(() -> new ResultNotFoundException("User does not found"));
        log.info("Updating location of user {} with values {} and {}", user.getName(), longitude, longitude);
        user.setLatitude(latitude);
        user.setLongitude(longitude);
        return usersRepository.save(user);
    }
}
