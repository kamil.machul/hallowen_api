package com.onwelo.perla.lublin.hackaton.controller;

import com.onwelo.perla.lublin.hackaton.exceptions.ResultNotFoundException;
import com.onwelo.perla.lublin.hackaton.mapper.UserMapper;
import com.onwelo.perla.lublin.hackaton.model.dto.UserDto;
import com.onwelo.perla.lublin.hackaton.model.entity.Role;
import com.onwelo.perla.lublin.hackaton.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.onwelo.perla.lublin.hackaton.model.entity.Role.ROLE_MONSTER;

@RestController
@CrossOrigin
@RequestMapping("users")
public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping
    public List<UserDto> getAll() {
        return usersService.findAll().stream()
                .map(UserMapper.INSTANCE::userToUserDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/id/{id}")
    public UserDto getUser(@PathVariable Long id) throws ResultNotFoundException {
        return UserMapper.INSTANCE.userToUserDto(usersService.getUser(id));
    }

    @PostMapping()
    public ResponseEntity<UserDto> addUser(@RequestParam String name,
                                           @RequestParam Float longitude,
                                           @RequestParam Float latitude,
                                           @RequestParam int power) {
        return ResponseEntity.ok().body(UserMapper.INSTANCE.userToUserDto(
                usersService.addUser(
                        UserDto.builder()
                                .name(name)
                                .role(Role.ROLE_USER)
                                .longitude(longitude)
                                .latitude(latitude)
                                .power(power)
                                .healthPoints(100)
                                .build())));
    }

    @PostMapping("monster/name/{name}")
    public ResponseEntity<UserDto> addMonster(@PathVariable String name) throws Exception {
        return ResponseEntity.ok().body(UserMapper.INSTANCE.userToUserDto(
                usersService.addMonster(
                        UserDto.builder()
                                .name(name)
                                .role(ROLE_MONSTER)
                                .healthPoints(10000)
                                .build())));
    }

    @GetMapping("monster")
    public ResponseEntity<UserDto> getMonster() {
        return ResponseEntity.ok().body(
                usersService.findAll()
                        .stream()
                        .filter(user -> ROLE_MONSTER == user.getRole())
                        .findFirst()
                        .map(UserMapper.INSTANCE::userToUserDto)
                        .orElse(null)
        );
    }

    @PutMapping("/id/{id}")
    public ResponseEntity<UserDto> updateUserLocation(@PathVariable Long id,
                                                      @RequestParam Float longitude,
                                                      @RequestParam Float latitude) throws ResultNotFoundException {
        return ResponseEntity.ok().body(UserMapper.INSTANCE.userToUserDto(
                usersService.updateLocation(id, longitude, latitude)));
    }

}
