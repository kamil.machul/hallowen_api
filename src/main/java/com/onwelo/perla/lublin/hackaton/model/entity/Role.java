package com.onwelo.perla.lublin.hackaton.model.entity;

public enum Role {
    ROLE_USER, ROLE_MONSTER;
}
