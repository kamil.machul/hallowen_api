package com.onwelo.perla.lublin.hackaton.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "users")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Role role;

    @Column
    private int power;

    @Column
    private float longitude;

    @Column
    private float latitude;

    @Column
    private int healthPoints;
}
