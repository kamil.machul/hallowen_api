package com.onwelo.perla.lublin.hackaton.service;

import com.onwelo.perla.lublin.hackaton.exceptions.ResultNotFoundException;
import com.onwelo.perla.lublin.hackaton.model.entity.Event;
import com.onwelo.perla.lublin.hackaton.model.entity.EventType;
import com.onwelo.perla.lublin.hackaton.model.entity.Role;
import com.onwelo.perla.lublin.hackaton.model.entity.User;
import com.onwelo.perla.lublin.hackaton.repository.EventRepository;
import com.onwelo.perla.lublin.hackaton.repository.UsersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class EventService {
    private final EventRepository eventRepository;
    private final UsersRepository usersRepository;


    public EventService(EventRepository eventRepository, UsersRepository usersRepository) {
        this.eventRepository = eventRepository;
        this.usersRepository = usersRepository;
    }

    public Event addEvent(Long reporterId, EventType eventType, int multiplier, String description, Float longitude,
                          Float latitude) throws ResultNotFoundException {
        User reporter = usersRepository.findById(reporterId).orElseThrow(() -> new ResultNotFoundException("User does not found"));
        Event event = Event.builder()
                .eventType(eventType)
                .multiplier(multiplier)
                .description(description)
                .isActive(true)
                .longitude(longitude)
                .latitude(latitude)
                .reporter(reporter)
                .build();
        if (event.getEventType().equals(EventType.SINGLE)) {
            log.info("Event is type of SINGLE, ending it now");
            event.setActive(false);
            attackMonster(event);
        }
        return eventRepository.save(event);
    }

    public Event addMemberToEvent(Long memberId, Long eventId) throws ResultNotFoundException {
        User member = usersRepository.findById(memberId).orElseThrow(() -> new ResultNotFoundException("User does not found"));
        Event event = eventRepository.findById(eventId).orElseThrow(() -> new ResultNotFoundException("Event does not found"));
        if (event.getReporter().equals(member)) {
            throw new IllegalArgumentException("Reporter can't join as a member");
        } else if (!event.isActive()) {
            throw new IllegalArgumentException("Cannot update ended event");
        }
        log.info("Adding new member {} to event {}", member.getName(), event.getDescription());

        event.getMembers().add(member);
        if (event.getEventType().equals(EventType.DOUBLE)) {
            log.info("Event is type of DOUBLE, adding second member is ending this event");
            event.setActive(false);
            attackMonster(event);
            return eventRepository.save(event);
        }
        log.info("Event is type of MULTIPLE, it will be ended by manual action");
        return eventRepository.save(event);
    }

    public Event finishEvent(Long requesterId, Long eventId) throws ResultNotFoundException {
        User user = usersRepository.findById(requesterId).orElseThrow(() -> new ResultNotFoundException("User does not found"));
        Event event = eventRepository.findById(eventId).orElseThrow(() -> new ResultNotFoundException("Event does not found"));
        if (!event.getReporter().equals(user)) {
            throw new IllegalArgumentException("Requester is not reporter of event, finishing event is forbidden");
        } else if (!event.isActive()) {
            throw new IllegalArgumentException("Cannot update ended event");
        } else if (!event.getEventType().equals(EventType.MULTIPLE)) {
            throw new IllegalArgumentException("Only MULTIPLE events can be ended manually");
        }
        log.info("Finishing event {} by user {}", event.getDescription(), user.getName());
        event.setActive(false);
        attackMonster(event);
        return eventRepository.save(event);
    }

    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    private int computeOverallDamage(Event event) {
        log.info("Calculating overall damage for event {}", event.getDescription());
        int sumOfPower = event.getReporter().getPower() +
                (Objects.isNull(event.getMembers()) ? 0 : event.getMembers().stream()
                        .map(User::getPower)
                        .reduce(0, Integer::sum));
        log.info("Calculated summary power {}", sumOfPower);
        int result = sumOfPower * event.getMultiplier();
        log.info("Calculated summary damage for multiplier {} : {}", event.getMultiplier(), result);
        return result;
    }

    private User attackMonster(Event event) throws ResultNotFoundException {
        User user = usersRepository.findUsersByRole(Role.ROLE_MONSTER).stream()
                .findFirst().orElseThrow(() -> new ResultNotFoundException("User does not found"));
        user.setHealthPoints(user.getHealthPoints() - computeOverallDamage(event));
        return usersRepository.save(user);
    }
}
