package com.onwelo.perla.lublin.hackaton.exceptions;

public class DuplicatedResourceException extends Exception{
    public DuplicatedResourceException(String message) {
        super(message);
    }
}
