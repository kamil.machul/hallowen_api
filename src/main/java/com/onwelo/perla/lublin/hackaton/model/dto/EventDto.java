package com.onwelo.perla.lublin.hackaton.model.dto;

import com.onwelo.perla.lublin.hackaton.model.entity.EventType;
import com.onwelo.perla.lublin.hackaton.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventDto {
    private Long id;
    private EventType eventType;
    private String description;
    private int multiplier;
    private boolean isActive;
    private float longitude;
    private float latitude;
    private User reporter;
    private List<User> members;
}
