package com.onwelo.perla.lublin.hackaton.repository;

import com.onwelo.perla.lublin.hackaton.model.entity.Role;
import com.onwelo.perla.lublin.hackaton.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UsersRepository extends JpaRepository<User, Long> {

    boolean existsUsersByRole(Role role);

    List<User> findUsersByRole(Role role);
}
