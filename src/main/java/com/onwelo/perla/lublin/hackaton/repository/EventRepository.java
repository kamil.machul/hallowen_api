package com.onwelo.perla.lublin.hackaton.repository;

import com.onwelo.perla.lublin.hackaton.model.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event, Long> {
}
