package com.onwelo.perla.lublin.hackaton.exceptions;

public class ResultNotFoundException extends Exception{
    public ResultNotFoundException(String message) {
        super(message);
    }
}
