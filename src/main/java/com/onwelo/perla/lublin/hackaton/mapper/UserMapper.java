package com.onwelo.perla.lublin.hackaton.mapper;

import com.onwelo.perla.lublin.hackaton.model.dto.UserDto;
import com.onwelo.perla.lublin.hackaton.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto userToUserDto(User user);

    User userDtoToUser(UserDto userDto);

    List<UserDto> usersToUsersDto(List<User> user);

    List<User> usersDtoToUsers(List<UserDto> userDto);
}
