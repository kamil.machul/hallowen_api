package com.onwelo.perla.lublin.hackaton.model.dto;

import com.onwelo.perla.lublin.hackaton.model.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto {
    private Long id;
    private String name;
    private Role role;
    private int power;
    private float longitude;
    private float latitude;
    private int healthPoints;
}
