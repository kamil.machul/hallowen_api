package com.onwelo.perla.lublin.hackaton.mapper;

import com.onwelo.perla.lublin.hackaton.model.dto.EventDto;
import com.onwelo.perla.lublin.hackaton.model.entity.Event;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = UserMapper.class)
public interface EventMapper {
    EventMapper INSTANCE = Mappers.getMapper(EventMapper.class);


    public EventDto eventToEventDto(Event event);

    public Event eventDtoEvent(EventDto event);
}
