FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} hackaton-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/hackaton-0.0.1-SNAPSHOT.jar"]